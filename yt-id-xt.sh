# MIT Licensed script made by Darwin Brandão
# It uses cURL and xmllint to get the YouTube channel's ID
curl -s $1 | xmllint --html --xpath 'string(//meta[@itemprop="channelId"]/@content)' - 2>/dev/null
